== Lista de materiales

// Materiales necesarios por categoría, ordenados alfabéticamente

=== Cocina y comida

* Servilletas
* Tenedores
* Garrotazo +15
* Carbón
* Pinzas
* Platos

=== Mobiliario

* Mesas
* Sillas

=== Diversión

* Radiocassette
* Ouija
* Altavoces
* Plastilina

=== Componentes electrónicos

* Un pc cada uno con periféricos incluidos
* Un generador eléctrico
* switch con wifi
* Test antigenos
